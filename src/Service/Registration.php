<?php


namespace App\Service;


use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Registration
{
    private User $user;
    private UserPasswordEncoderInterface $encoderService;

    public function __construct(User $user, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->user = $user;
        $this->encoderService = $passwordEncoder;
    }

    public function registrationProcess(array $postedData) : bool
    {
        $this->user->setPassword(
            $this->encoderService->encodePassword(
                $this->user,
                $postedData
            )
        );

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($this->user);
        $entityManager->flush();
        // do anything else you need here, like send an email
        return true;
    }
}